# -*- coding: utf-8 -*-
from odoo import http


class Ragnar(http.Controller):
    @http.route('/ragnar/ragnar/', auth='public')
    def index(self, **kw):
        return "Hello, world"

    @http.route('/ragnar/ragnar/objects/', auth='public')
    def list(self, **kw):
        return http.request.render('ragnar.listing', {
            'root': '/ragnar/ragnar',
            'objects': http.request.env['ragnar.ragnar'].search([]),
        })

    @http.route('/ragnar/ragnar/objects/<model("ragnar.ragnar"):obj>/', auth='public')
    def object(self, obj, **kw):
        return http.request.render('ragnar.object', {
            'object': obj
        })
