from odoo.tests.common import TransactionCase


class TestApplication(TransactionCase):

    def setUp(self, *args, **kwargs):
        result = super().setUp(*args, **kwargs)
        self.Application = self.env['ragnar.application']
        self.app1 = self.Application.create({
            'name': 'Application 1',
            'description': 'A simple test application.'})
        return result

    def test_create(self):
        "Test application components is empty by default."
        self.assertEqual(len(self.app1.component_ids), 0)
