# -*- coding: utf-8 -*-
{
    'name': "Data Privacy",

    'summary': """
        An organization-wide data privacy management solution.""",

    'description': """
        This module provides the tools required by a DPO to handle registers for personal data
        storage and processing.
    """,

    'author': "Alcibiade Corp.",
    'website': "http://www.alcibiade.org/",
    'application': True,

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'project', 'mail'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'data/vulnerability_stage.xml',
    ],
    # only loaded in demonstrat:w
    # ion mode
    'demo': [
        'demo/demo.xml',
    ],
}
