# -*- coding: utf-8 -*-

from odoo import models, fields


class DataStore(models.Model):
    _name = 'privacy.data.store'
    _description = 'A personal data storage system.'
    _inherit = ['mail.thread']

    name = fields.Char(string='Name', tracking=True)
    description = fields.Text(string='Description', tracking=True)

    process_ids = fields.Many2many('privacy.data.process', string='Processes')


class DataProcess(models.Model):
    _name = 'privacy.data.process'
    _description = 'A personal data process.'
    _inherit = ['mail.thread']

    name = fields.Char(tracking=True)
    description = fields.Text(tracking=True)

    store_ids = fields.Many2many('privacy.data.store', string='Stores')
