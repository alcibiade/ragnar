[options]
admin_passwd={{.Env.ADMIN_PASSWORD}}
list_db={{.Env.LIST_DB}}

db_host={{.Env.DB_HOST}}
db_port={{.Env.DB_PORT}}

db_name={{.Env.DB_NAME}}

db_user={{.Env.DB_USER}}
db_password={{.Env.DB_PASSWORD}}
