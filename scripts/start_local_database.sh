#!/bin/bash

docker run -d --rm --name db -p5432:5432 -e POSTGRES_USER=odoo -e POSTGRES_PASSWORD=odoo -e POSTGRES_DB=odoo postgres:11
