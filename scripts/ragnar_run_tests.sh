#!/usr/bin/env bash
set -e -x

TMP_BUFFER=/tmp/ragnar_$$

# Display tables in the odoo database, for debugging, should be empty on startup

echo "select table_catalog, table_schema, table_name \
      from information_schema.tables \
      where table_schema = 'public'\
      order by table_name" | su - postgres -c 'psql odoo'

# Create an instance named 'db1'

curl 'http://localhost:8069/web/database/create' \
    -H 'Content-Type: application/x-www-form-urlencoded'                                          \
    --data 'master_pwd=admin&name=db1&login=admin&password=admin&phone=&lang=en_US&country_code=' \
    > /dev/null

# Display all tables in db1 database for debugging

echo "select table_catalog, table_schema, table_name\
      from information_schema.tables\
      where table_schema = 'public'\
      order by table_name" | su - postgres -c 'psql db1'

# Assert that ragnar tables have been created and initialization was successful

echo "select table_catalog, table_schema, table_name               \
      from information_schema.tables                               \
      where table_schema = 'public' and table_name like 'ragnar_%' \
      order by table_name" | su - postgres -c 'psql db1' | tee ${TMP_BUFFER}

cat ${TMP_BUFFER} | grep db1 > /dev/null # This asserts presence of at least one module table
